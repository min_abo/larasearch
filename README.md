# larasearch

#### 介绍
laravel 快速易用搜索项目
```js
人无完人,开源项目的发展离不开 踩坑&填坑. 
可以 提issue等Abo解决 / 自己动手解决问题.贡献自己的力量比抱怨帅气多了

No one is perfect, the development of open source projects can not be separated from the pit and fill pit. 
You can publish issue and wait for solution.
Or do-it-yourself problem solving. It's more handsome than complaining about.
```

#### 安装教程
安装
```composer log
composer require abo/larasearch
```

#### 使用说明

发布项目配置文件
```js
php artisan vendor:publish --provider="Abo\Larasearch\LarasearchServiceProvider"
```

如有需要数据同步脚本,还可以执行以下命令生成
```js
php artisan larasearch:sync {table_name}
```

#### 快速开始

新建索引数据
```js
Route::get('test/create',function(\Abo\Larasearch\EsBuilder $builder){
    $result = $builder->index('index')->type('type')->create([
        'key' => 'value',
    ]);
    dump($result);
});
```

更新索引数据
```js
Route::get('test/create',function(\Abo\Larasearch\EsBuilder $builder){
    $result = $builder->index('index')->type('type')->update('id',[
        'key' => 'value2',
    ]);
    dump($result);
});
```

删除索引
```js
Route::get('test/create',function(\Abo\Larasearch\EsBuilder $builder){
    $result = $builder->index('index')->type('type')->delete('id');
    dump($result);
});
```

查询索引数据
```js
Route::get('test/create',function(\Abo\Larasearch\Builder $builder){
    $builder = $builder->index('index')->type('type');
    
    //SQL:select ... where id = 1 limit 1;
    $result = $builder->whereTerm('id',1)->first();    
});
```
#### 更多用法
增删改
```js
public function create(array $data, $id = null, $key = 'id'): stdClass

public function update($id, array $data): bool

public function delete($id)
```

获取查询对象
```js
getElasticSearch()
```

#### 查询字段
public function select($columns): self

#### 条件
精准查询
```js
$builder->whereTerm('key',value)->first();
$builder->whereIn('key',[value1,value2])->first();
```

模糊(分词)查询
```js
$builder->whereMatch('key',value)->first();
```

范围查询
```js
$builder->whereBetween('key',[value1,value2])->first();
```

复合查询
```js
//SQL:select ... where (key=1 or key=2) and key1=1

$result = $builder->where(function (Builder $Query) {
    $Query->whereTerm('key',1)->orWhereTerm('key',2)
})->whereTerm('key1',1)->get();
```

#### 分页
offset / limit
```js
$builder->limit(10)->get(); 
$builder->offset(10)->limit(10)->get(); 
```
paginate
```js
public function paginate(int $page, int $perPage = 15): Collection
```
scroll
```js
public function scroll(string $scroll): self
```

#### 排序 / 聚合

排序 asc/desc
```js
public function orderBy(string $field, $sort): self
```

聚合(待增强)
```js
public function aggBy($field, $type): self
```

#### 结果

获取全部
```js
public function get(): Collection
```

获取第一条
```js
public function first()
```

通过id获取(索引较多时会出现需要制定分片路由情况)
```js
public function byId($id)

public function byIdOrFail($id): stdClass
```
chunk
```js
public function chunk(callable $callback, $limit = 2000, $scroll = '10m')
```
count
```js
public function delete($id)
```

日志
```js
#但其实,开启 config/larasearch.php 
    'open_log' => true,
每次查询都会有记录

//开启日志
$builder->enableQueryLog();

//打印所有查询
dump($build->getQueryLog());

//最新查询日志
dump($build->getLastQueryLog());
```
