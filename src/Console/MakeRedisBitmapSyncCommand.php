<?php

namespace Abo\Larasearch\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Abo\Generalutil\V1\Utils\StringUtil;
use Abo\Larasearch\Console\CommandAutoClientLogic;

class MakeRedisBitmapSyncCommand extends Command
{
    /** The name and signature of the console command. @var string */
    protected $signature = 'larasearch:sync {name}';

    protected $name = 'larasearch:sync';

    /** The console command description. @var string */
    protected $description = 'create larasearch sync data logic';

    /** Execute the console command. @return mixed */
    public function handle()
    {
        // parent::handle(); // 创建文件
        $this->createAppClientCommands();
    }

    /** 获取命令参数 @return array */
//    protected function getArguments()
//    {
//        return [
//            ['name', InputArgument::REQUIRED, 'The name of the synchronize data.'],
//        ];
//    }

    protected function createAppClientCommands()
    {
        $CommandAutoClientLogic = new CommandAutoClientLogic( trim($this->argument('name')) );

        $CommandAutoClientLogic->setTableSetting();         // 触发器,change_log表设置
        $CommandAutoClientLogic->createAppClientCommands(); // 同步命令添加
        $CommandAutoClientLogic->createAppClientLogic();    // 同步逻辑添加
        $CommandAutoClientLogic->createAppClientRepository(); // 同步模型操作添加

        return '完成客户端创建';
    }
}
