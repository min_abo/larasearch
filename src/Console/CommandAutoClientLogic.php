<?php
namespace Abo\Larasearch\Console;

use Abo\Generalutil\V1\Utils\FileUtil;
use Abo\Generalutil\V1\Utils\StringUtil;
use Abo\Larasearch\V0\SyncDatabase\Logic\SyncTableSettingLogic;
/**
 * 命令行自动生成 数据同步客户端
 * Description:
 * Class AutoClientCommandLogic
 * @package Abo\Larasearch\SyncDatabase\Logic
 */
class CommandAutoClientLogic
{
    protected $inputName, $logicName;

    public function __construct( string $inputName )
    {
        if ( !$inputName ) {
            throw new \Exception( 'table_name is require, retry input' );
        }

        $this->inputName = $inputName;
        $this->logicName = $this->tableName2ClassName();
    }

    /** 创建应用 数据变更日志 */
    public function setTableSetting()
    {
        /*
         CREATE TRIGGER `trig_mw_game_insert` AFTER INSERT ON `mw_game` FOR EACH ROW INSERT INTO mw_game_change_log ( `type`, `change_id` ) VALUES ( 'INSERT', NEW.id );
         CREATE TRIGGER `trig_mw_game_update` AFTER UPDATE ON `mw_game` FOR EACH ROW INSERT INTO mw_game_change_log ( `type`, `change_id` ) VALUES ( 'UPDATE', NEW.id );
         CREATE TRIGGER `trig_mw_game_delete` AFTER DELETE ON `mw_game` FOR EACH ROW INSERT INTO mw_game_change_log ( `type`, `change_id` ) VALUES ( 'DELETE', OLD.id );
         */
        $ret2SetTable = ( new SyncTableSettingLogic() )->setTableSetting( $this->inputName );

        shell_exec( 'php artisan make:model Model/'.$this->logicName.'ChangeLog' );
        echo "\r\n创建 数据变更日志表:".$ret2SetTable;
        return true;
    }

    /** 创建应用 同步 搜索索引数据 客户端命令 */
    public function createAppClientCommands()
    {
        $fileName = $this->logicName.'Index';
        $filePath = app_path( 'Console/Commands/Larasearch'.$fileName.'.php' );

        $cdBasePathCommand = 'cd '.base_path();
        shell_exec( $cdBasePathCommand );
        $createClientCommand = 'php artisan make:command Larasearch'.$fileName.' --command=search_sync:'.$this->inputName;
        shell_exec( $createClientCommand );


        $fileContent = file_get_contents( $filePath );
        $fileContent = str_replace(
            'public function handle()
    {',
            'public function handle()
    {
        // return ( new \App\Logic\Larasearch\\'.$this->logicName.'SyncLogic() )->syncData2Index(); // 变更同步
        return ( new \App\Logic\Larasearch\\'.$this->logicName.'SyncLogic() )->initData2Index(); // 初始化数据',
            $fileContent
        );

        $ret2CreateCommands = file_put_contents( $filePath, $fileContent );

        echo "\r\n创建 数据同步命令:".$ret2CreateCommands;
        return true;
    }

    /** 客户端 逻辑创建 */
    public function createAppClientLogic()
    {
        $createClassName = $this->logicName.'SyncLogic';

        $createStuFileConfig = [
            'stubPath' => __DIR__.'/stubs/ElasticSearchSyncDataLogic.stub',
            'createClassPath' => app_path( 'Logic/Larasearch/'. $createClassName .'.php' ),
            'targetArray' => [ 'DummyClass', 'DummyTable', 'DummyTClass' ],
            'replaceArray' => [ $createClassName, $this->inputName, $this->logicName ],
        ];

        $ret2CreateLogic = ( new FileUtil() )->createStubFile( $createStuFileConfig );

        echo "\r\n创建 数据同步逻辑:".$ret2CreateLogic;
        return true;
    }

    /** 客户端 模型操作类创建 */
    public function createAppClientRepository()
    {
        $createClassName = $this->logicName.'ChangeLogRepository';

        $createStuFileConfig = [
            'stubPath' => __DIR__.'/stubs/ElasticSearchChangeLogRepository.stub',
            'createClassPath' => app_path( 'Repositories/Larasearch/'. $createClassName .'.php' ),
            'targetArray' => [ 'DummyClass', 'DummyTable' ],
            'replaceArray' => [ $createClassName, $this->inputName ],
        ];

        $ret2CreateReosity = ( new FileUtil() )->createStubFile( $createStuFileConfig );

        echo "\r\n创建 数据同步模型操作类:".$ret2CreateReosity;
        return true;
    }

    /** 表名 转 类名 */
    private function tableName2ClassName()
    {
        $StringUtil = new StringUtil();
        return ucfirst( $StringUtil->camelize( $this->inputName ) );
    }
}